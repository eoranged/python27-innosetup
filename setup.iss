; -*- mode: iss; coding: cp1251 -*-

; ��� ����������. ��������� �� ���� ��������, ������������� ��� �������� ���� � Program Files
; � ��� ��� ������
#define MyAppName "Visualizer Ex"
; ������ ����������
#define MyAppVersion "1.0"
; �����
#define MyAppPublisher "My Company, Inc."
; ����
#define MyAppURL "http://www.example.com/"
; ������� ���������� ��� ������: ��� ��������� � ������� ��� �����������, ���� �� ������������� setup.exe
#define MySourceDir "Z:\enth"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{72ADD07C-939F-4E2C-B14C-A589EE0686E3}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\{#MyAppName}
DisableDirPage=yes
DefaultGroupName={#MyAppName}
DisableProgramGroupPage=yes
OutputDir={#MySourceDir}
OutputBaseFilename=setup
Compression=none
SolidCompression=yes
; ��������� ��������� ���������� ��������� ��� ������������
ChangesEnvironment=yes

[Languages]
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"

[Registry]
; ����������� � PATH ����� � ��� ��� �������
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Control\Session Manager\Environment"; ValueType: expandsz; ValueName: "Path"; ValueData: "{olddata};C:\Python27;C:\Python27\Scripts"; Check: NeedsAddEnv('Path', 'C:\Python27\Scripts')
; ����� �������� ��� ���� ���������� � PATH, ����� ����������� ���������� ������� �
;   �������� C:\Python27;C:\Python27\Scripts �� ����, ������� ����� ��������� �
;   � NeedsAddEnv('Path', 'C:\Python27\Scripts') ��������� ����, ������� �������� ����� ���������.
; ���� ������� ������������ ����������, �� �� ������� ����� "�����������" � ������ NeedsAddEnv ��� ���:
;   NeedsAddEnv('Path', ExpandConstant('{app}'))
; ����� �������� ������ ���������� ��������� (�������� PATHONPATH), ����� �������� ��� ���������
;   Path � ���� �������� �� ��� ������� ����������

[Code]
function NeedsAddEnv(VarName: string; Param: string): boolean;
var
  OrigValue: string;
begin
  if not RegQueryStringValue(HKEY_LOCAL_MACHINE,
    'SYSTEM\CurrentControlSet\Control\Session Manager\Environment',
    VarName, OrigValue)
  then begin
    Result := True;
    exit;
  end;
  // look for the path with leading and trailing semicolon
  // Pos() returns 0 if not found
  Result := Pos(';' + Param + ';', ';' + OrigValue + ';') = 0;
end;

[Files]
; ����������� �������
Source: "{#MySourceDir}\visualizerEx\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs
; ������� ���������� (vc++, python)
Source: "{#MySourceDir}\py-modules\vcredist_x64.exe"; DestDir: "{app}"
Source: "{#MySourceDir}\py-modules\python-2.7.9.amd64.msi"; DestDir: "{app}"

#include ".\files.inc"
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Run]
Filename: "{app}\vcredist_x64.exe"; Parameters: "/q"; StatusMsg: "��������������� Visual C++ 2008 Redistributable..."
Filename: "C:\Windows\System32\msiexec.exe"; Parameters: "/i ""{app}\python-2.7.9.amd64.msi"" /qn ADDLOCAL=DefaultFeature,Extensions,TclTk,Tools ALLUSERS=1"; StatusMsg: "��������������� Python 2.7"
Filename: "C:\Python27\python.exe"; Parameters: "-m ensurepip"; StatusMsg: "������������� Python 2.7"; Flags: runhidden
#include ".\run.inc"

;[UninstallRun]
;Filename: "{app}\vcredist_x64.exe"; Parameters: "/qu"; StatusMsg: "��������� Visual C++ 2008 Redistributable..."
;Filename: "C:\Windows\System32\msiexec.exe"; Parameters: "/x ""{app}\python-2.7.9.amd64.msi"" /qn"; StatusMsg: "��������� Python 2.7"

; ������ �� ������� ����
[Icons]
Name: "{commondesktop}\{#MyAppName}"; Filename: "C:\Python27\pythonw.exe"; Parameters: """{app}\visualizerWindow.py"""; WorkingDir: {app}
