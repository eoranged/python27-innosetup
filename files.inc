; -*- mode: iss; coding: cp1251 -*-
Source: "{#MySourceDir}\py-modules\apptools-4.2.1.tar.gz"; DestDir: "{tmp}"; Flags: deleteafterinstall
Source: "{#MySourceDir}\py-modules\configobj-5.0.6.tar.gz"; DestDir: "{tmp}"; Flags: deleteafterinstall
Source: "{#MySourceDir}\py-modules\mayavi-4.3.1+vtk510-cp27-none-win_amd64.whl"; DestDir: "{tmp}"; Flags: deleteafterinstall
Source: "{#MySourceDir}\py-modules\numpy-1.8.2+mkl-cp27-none-win_amd64.whl"; DestDir: "{tmp}"; Flags: deleteafterinstall
Source: "{#MySourceDir}\py-modules\pip-6.0.8.tar.gz"; DestDir: "{tmp}"; Flags: deleteafterinstall
Source: "{#MySourceDir}\py-modules\pyface-4.4.0.tar.gz"; DestDir: "{tmp}"; Flags: deleteafterinstall
Source: "{#MySourceDir}\py-modules\PyQt4-4.11.3-cp27-none-win_amd64.whl"; DestDir: "{tmp}"; Flags: deleteafterinstall
Source: "{#MySourceDir}\py-modules\pywin32-219-cp27-none-win_amd64.whl"; DestDir: "{tmp}"; Flags: deleteafterinstall
Source: "{#MySourceDir}\py-modules\six-1.9.0.tar.gz"; DestDir: "{tmp}"; Flags: deleteafterinstall
Source: "{#MySourceDir}\py-modules\traits-4.5.0-cp27-none-win_amd64.whl"; DestDir: "{tmp}"; Flags: deleteafterinstall
Source: "{#MySourceDir}\py-modules\traitsui-4.4.0.tar.gz"; DestDir: "{tmp}"; Flags: deleteafterinstall
Source: "{#MySourceDir}\py-modules\pip-6.0.8.tar.gz"; DestDir: "{tmp}"; Flags: deleteafterinstall
Source: "{#MySourceDir}\py-modules\VTK-5.10.1-cp27-none-win_amd64.whl"; DestDir: "{tmp}"; Flags: deleteafterinstall
Source: "{#MySourceDir}\py-modules\wxPython-2.8.10.1-7.egg"; DestDir: "{tmp}"; Flags: deleteafterinstall

Source: "{#MySourceDir}\py-modules\matplotlib-1.4.3-cp27-none-win_amd64.whl"; DestDir: "{tmp}"; Flags: deleteafterinstall
Source: "{#MySourceDir}\py-modules\python_dateutil-2.4.1-py2.py3-none-any.whl"; DestDir: "{tmp}"; Flags: deleteafterinstall
Source: "{#MySourceDir}\py-modules\pytz-2014.10-py2.py3-none-any.whl"; DestDir: "{tmp}"; Flags: deleteafterinstall
Source: "{#MySourceDir}\py-modules\pyparsing-2.0.3-py2-none-any.whl"; DestDir: "{tmp}"; Flags: deleteafterinstall
